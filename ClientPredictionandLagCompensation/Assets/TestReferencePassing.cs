﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class TestReferencePassing : MonoBehaviour
{
    public enum TestCase { Test1, Test2 }

    public TestCase Case;


    public class TestClassA
    {
        public string Name
        {
            get;
            set;
        }
        public TestClassA()
        {
            Name = "Default";
        }
    }

    Action<object> OurCallback;
    Action OurCallback2;
    object OurParameter;

    // Start is called before the first frame update
    void Start()
    {
        switch (Case)
        {
            case TestCase.Test1:
                TestClassA a1 = new TestClassA();
                a1.Name = "a1";

                ////Begin test with parameter
                RegisterCallbackFunction((obj) =>
                {
                    //TestClassA objAsTestClassA = (TestClassA)obj;
                    TestClassA objAsTestClassA = obj as TestClassA;                    
                    Debug.Log(objAsTestClassA.Name);
                }, a1);
                a1 = new TestClassA();
                a1.Name = "Modified A1";
                //DO STUFF
                Debug.Log("Did STUFF");
                    //someObj.StuffFinished += OnStuffFinished;
                OnStuffFinished();
                break;
            case TestCase.Test2:

                //Begin closure test
                Debug.Log("Begin Closure Test");
                TestClassA closureTCA = new TestClassA();
                closureTCA.Name = "ClosureName";
                RegisterCallbackFunction(() =>
                {

                    Debug.Log(closureTCA.Name);
                });
                closureTCA = new TestClassA();
                closureTCA.Name = "ModifiedClosure";

                //DO STUFF
                Debug.Log("Did STUFF");
                OnStuffFinished2();
                break;
            default:
                break;
        }

    }

    void function (int a)
    {

    }

    void RegisterCallbackFunction(Action<object> callback, object parameter)
    {
        OurCallback = callback;
        OurParameter = parameter;
    }

    void RegisterCallbackFunction(Action<object> callback)
    {
        OurCallback = callback;
    }

    void RegisterCallbackFunction(Action callback)
    {
        OurCallback2 = callback;
    }

    void OnStuffFinished()
    {
        OurCallback(OurParameter);
    }

    void OnStuffFinished2()
    {
        OurCallback2();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
