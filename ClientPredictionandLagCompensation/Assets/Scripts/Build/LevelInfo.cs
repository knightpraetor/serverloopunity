﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelInfo", menuName = "Test/Level/LevelInfo")]
public class LevelInfo : ScriptableObject
{
    public Object MainScene;
    public bool IncludeInBuild = true;

    public LevelType Type = LevelType.Gameplay;
    public enum LevelType
    {
        Generic,
        Gameplay,
        Menu
    }

   
}
