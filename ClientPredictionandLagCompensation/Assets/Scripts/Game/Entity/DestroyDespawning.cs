﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Entities;

[DisableAutoCreation]
public class DestroyDespawning : ComponentSystem
{
    ComponentGroup Group;

    protected override void OnCreateManager()
    {
        base.OnCreateManager();
        Group = GetComponentGroup(typeof(DespawningEntity));
    }

    protected override void OnUpdate()
    {
        var entityArray = Group.GetEntityArray();
        for (var i = 0; i < entityArray.Length; i++)
        {
            PostUpdateCommands.DestroyEntity(entityArray[i]);
        }
    }
}
