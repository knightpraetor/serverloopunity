﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class ServerGameLoop : Game.IGameLoop
{
    NetworkServer m_NetworkServer;
    GameWorld m_GameWorld;
    //NetworkStatisticsServer m_NetworkStatistics;
    //NetworkCompressionModel m_Model = NetworkCompressionModel.DefaultModel;

    SocketTransport m_NetworkTransport;

    BundledResourceManager m_resourceSystem;
    //ChatSystemServer m_ChatSystem;
    Dictionary<int, ClientInfo> m_Clients = new Dictionary<int, ClientInfo>();

    //ServerGameWorld m_serverGameWorld;

    public double m_nextTickTime = 0;
    string m_RequestedGameMode = "deathmatch";

    long m_SimStartTime;
    int m_SimStartTimeTick;
    float m_LastSimTime;
    bool m_performLateUpdate;

    //SQPServer m_ServerQueryProtocolServer;

    [ConfigVar(Name = "show.gameloopinfo", DefaultValue = "0", Description = "Show gameloop info")]
    static ConfigVar showGameLoopInfo;

    [ConfigVar(Name = "server.quitwhenempty", DefaultValue = "0", Description = "If enabled, quit when last client disconnects.")]
    static ConfigVar serverQuitWhenEmpty;

    [ConfigVar(Name = "server.recycleinterval", DefaultValue = "0", Description = "Exit when N seconds old AND when 0 players. 0 means never.")]
    static ConfigVar serverRecycleInterval;

    [ConfigVar(Name = "debug.servertickstats", DefaultValue = "0", Description = "Show stats about how many ticks we run per Unity update (headless only)")]
    static ConfigVar debugServerTickStats;

    [ConfigVar(Name = "server.maxclients", DefaultValue = "8", Description = "Maximum allowed clients")]
    public static ConfigVar serverMaxClients;

    [ConfigVar(Name = "server.disconnecttimeout", DefaultValue = "30000", Description = "Timeout in ms. Server will kick clients after this interval if nothing has been heard.")]
    public static ConfigVar serverDisconnectTimeout;

    [ConfigVar(Name = "server.servername", DefaultValue = "", Description = "Servername")]
    static ConfigVar serverServerName;

    float m_ServerStartTime;
    int m_MaxClients;

    public void FixedUpdate()
    {
        throw new NotImplementedException();
    }

    public bool Init(string[] args)
    {
        throw new NotImplementedException();
    }

    public void LateUpdate()
    {
        throw new NotImplementedException();
    }

    public void Shutdown()
    {
        throw new NotImplementedException();
    }

    public void Update()
    {
        throw new NotImplementedException();
    }
}

