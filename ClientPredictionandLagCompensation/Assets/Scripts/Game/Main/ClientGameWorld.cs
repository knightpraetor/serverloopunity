﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;

public class ClientGameWorld
{

    public bool PredictionEnabled = true;

    public float frameTimeScale = 1.0f;

    public GameTime PredictedTime
    {
        get { return m_PredictedTime; }
    }

    public GameTime RenderTime
    {
        get { return m_RenderTime; }
    }

    //public ReplicatedEntityModuleClient ReplicatedEntityModule
    //{
    //    get { return m_ReplicatedEntityModule; }
    //}

    GameWorld m_GameWorld;
    GameTime m_PredictedTime = new GameTime(60);
    GameTime m_RenderTime = new GameTime(60);

    //// External systems
    //NetworkClient m_NetworkClient;
    //NetworkStatisticsClient m_NetworkStatistics;
    //ClientFrontendUpdate m_ClientFrontendUpdate;

    //// Internal systems
    //LocalPlayer m_localPlayer;



    //readonly CharacterModuleClient m_CharacterModule;
    //readonly ProjectileModuleClient m_ProjectileModule;
    //readonly HitCollisionModule m_HitCollisionModule;
    //readonly PlayerModuleClient m_PlayerModule;
    //readonly SpectatorCamModuleClient m_SpectatorCamModule;
    //readonly EffectModuleClient m_EffectModule;
    //readonly ReplicatedEntityModuleClient m_ReplicatedEntityModule;
    //readonly ItemModule m_ItemModule;

    //readonly RagdollModule m_ragdollSystem;
    //readonly GameModeSystemClient m_GameModeSystem;

    //readonly ApplyGrenadePresentation m_ApplyGrenadePresentation;

    //readonly HandlePresentationOwnerDesawn m_HandlePresentationOwnerDespawn;
    //readonly UpdatePresentationOwners m_UpdatePresentationOwners;

    //readonly TwistSystem m_TwistSystem;
    //readonly FanSystem m_FanSystem;
    //readonly TranslateScaleSystem m_TranslateScaleSystem;

    //readonly MoverUpdate m_moverUpdate;
    //readonly DestructiblePropSystemClient m_DestructiblePropSystemClient;
    //readonly HandleNamePlateSpawn m_HandleNamePlateOwnerSpawn;
    //readonly HandleNamePlateDespawn m_HandleNamePlateOwnerDespawn;

    //readonly UpdateNamePlates m_UpdateNamePlates;
    //readonly SpinSystem m_SpinSystem;
    //readonly TeleporterSystemClient m_TeleporterSystemClient;




    //public ClientGameWorld(GameWorld world, NetworkClient networkClient, NetworkStatisticsClient networkStatistics, BundledResourceManager resourceSystem)
    //{
    //    //m_NetworkClient = networkClient;
    //    //m_NetworkStatistics = networkStatistics;

    //    //m_GameWorld = world;

    //    //m_CharacterModule = new CharacterModuleClient(m_GameWorld, resourceSystem);
    //    //m_ProjectileModule = new ProjectileModuleClient(m_GameWorld, resourceSystem);
    //    //m_HitCollisionModule = new HitCollisionModule(m_GameWorld, 1, 1);
    //    //m_PlayerModule = new PlayerModuleClient(m_GameWorld);
    //    //m_SpectatorCamModule = new SpectatorCamModuleClient(m_GameWorld);
    //    //m_EffectModule = new EffectModuleClient(m_GameWorld, resourceSystem);
    //    //m_ReplicatedEntityModule = new ReplicatedEntityModuleClient(m_GameWorld, resourceSystem);
    //    //m_ItemModule = new ItemModule(m_GameWorld);
    //    //m_ragdollSystem = new RagdollModule(m_GameWorld);

    //    //m_GameModeSystem = m_GameWorld.GetECSWorld().CreateManager<GameModeSystemClient>(m_GameWorld);

    //    //m_ClientFrontendUpdate = m_GameWorld.GetECSWorld().CreateManager<ClientFrontendUpdate>(m_GameWorld);

    //    //m_DestructiblePropSystemClient = m_GameWorld.GetECSWorld().CreateManager<DestructiblePropSystemClient>(m_GameWorld);

    //    //m_ApplyGrenadePresentation = m_GameWorld.GetECSWorld().CreateManager<ApplyGrenadePresentation>(m_GameWorld);

    //    //m_UpdatePresentationOwners = m_GameWorld.GetECSWorld().CreateManager<UpdatePresentationOwners>(
    //    //    m_GameWorld, resourceSystem);
    //    //m_HandlePresentationOwnerDespawn = m_GameWorld.GetECSWorld().CreateManager<HandlePresentationOwnerDesawn>(m_GameWorld);

    //    //m_moverUpdate = m_GameWorld.GetECSWorld().CreateManager<MoverUpdate>(m_GameWorld);

    //    //m_TeleporterSystemClient = m_GameWorld.GetECSWorld().CreateManager<TeleporterSystemClient>(m_GameWorld);

    //    //m_SpinSystem = m_GameWorld.GetECSWorld().CreateManager<SpinSystem>(m_GameWorld);

    //    //m_HandleNamePlateOwnerSpawn = m_GameWorld.GetECSWorld().CreateManager<HandleNamePlateSpawn>(m_GameWorld);
    //    //m_HandleNamePlateOwnerDespawn = m_GameWorld.GetECSWorld().CreateManager<HandleNamePlateDespawn>(m_GameWorld);
    //    //m_UpdateNamePlates = m_GameWorld.GetECSWorld().CreateManager<UpdateNamePlates>(m_GameWorld);

    //    //m_GameModeSystem.SetLocalPlayerId(m_NetworkClient.clientId);

    //    //m_TwistSystem = new TwistSystem(m_GameWorld);
    //    //m_FanSystem = new FanSystem(m_GameWorld);
    //    //m_TranslateScaleSystem = new TranslateScaleSystem(m_GameWorld);
    //}

    public void Shutdown()
    {
        //m_CharacterModule.Shutdown();
        //m_ProjectileModule.Shutdown();
        //m_HitCollisionModule.Shutdown();
        //m_PlayerModule.Shutdown();
        //m_SpectatorCamModule.Shutdown();
        //m_EffectModule.Shutdown();
        //m_ReplicatedEntityModule.Shutdown();
        //m_ItemModule.Shutdown();

        //m_GameWorld.GetECSWorld().DestroyManager(m_GameModeSystem);
        //m_GameWorld.GetECSWorld().DestroyManager(m_DestructiblePropSystemClient);

        //m_GameWorld.GetECSWorld().DestroyManager(m_ApplyGrenadePresentation);

        //m_GameWorld.GetECSWorld().DestroyManager(m_UpdatePresentationOwners);
        //m_GameWorld.GetECSWorld().DestroyManager(m_HandlePresentationOwnerDespawn);

        //m_GameWorld.GetECSWorld().DestroyManager(m_moverUpdate);

        //m_GameWorld.GetECSWorld().DestroyManager(m_TeleporterSystemClient);
        //m_GameWorld.GetECSWorld().DestroyManager(m_SpinSystem);
        //m_GameWorld.GetECSWorld().DestroyManager(m_HandleNamePlateOwnerSpawn);
        //m_GameWorld.GetECSWorld().DestroyManager(m_HandleNamePlateOwnerDespawn);
        //m_GameWorld.GetECSWorld().DestroyManager(m_UpdateNamePlates);

        //m_ragdollSystem.Shutdown();

        //m_TwistSystem.ShutDown();
        //m_FanSystem.ShutDown();
        //m_TranslateScaleSystem.ShutDown();
    }


    // This is called at the actual client frame rate, so may be faster or slower than tickrate.
    public void Update(float frameDuration)
    {
        //// Advances time and accumulate input into the UserCommand being generated
        //HandleTime(frameDuration);
//        m_GameWorld.worldTime = m_RenderTime;
//        m_GameWorld.frameDuration = frameDuration;
//        m_GameWorld.lastServerTick = m_NetworkClient.serverTime;

//        m_PlayerModule.ResolveReferenceFromLocalPlayerToPlayer();
//        m_PlayerModule.HandleCommandReset();
//        m_ReplicatedEntityModule.UpdateControlledEntityFlags();


//        // Handle spawn requests
//        m_ProjectileModule.HandleProjectileRequests();
//        m_UpdatePresentationOwners.Update();

//        // Handle spawning  
//        m_CharacterModule.HandleSpawns();
//        m_ProjectileModule.HandleProjectileSpawn();
//        m_HitCollisionModule.HandleSpawning();
//        m_HandleNamePlateOwnerSpawn.Update();
//        m_ragdollSystem.HandleSpawning();
//        m_TwistSystem.HandleSpawning();
//        m_FanSystem.HandleSpawning();
//        m_TranslateScaleSystem.HandleSpawning();
//        m_PlayerModule.HandleSpawn();
//        m_ItemModule.HandleSpawn();

//        // Handle controlled entity changed
//        m_PlayerModule.HandleControlledEntityChanged();
//        m_CharacterModule.HandleControlledEntityChanged();

//        // Update movement of scene objects. Projectiles and grenades can also start update as they use collision data from last frame
//        m_SpinSystem.Update();
//        m_moverUpdate.Update();
//        m_CharacterModule.Interpolate();
//        m_ReplicatedEntityModule.Interpolate(m_RenderTime);

//        // Prediction
//        m_GameWorld.worldTime = m_PredictedTime;
//        m_ProjectileModule.StartPredictedMovement();

//        if (IsPredictionAllowed())
//        {
//            // ROLLBACK. All predicted entities (with the ServerEntity component) are rolled back to last server state 
//            m_GameWorld.worldTime.SetTime(m_NetworkClient.serverTime, m_PredictedTime.tickInterval);
//            PredictionRollback();


//            // PREDICT PREVIOUS TICKS. Replay every tick *after* the last tick we have from server up to the last stored command we have
//            for (var tick = m_NetworkClient.serverTime + 1; tick < m_PredictedTime.tick; tick++)
//            {
//                m_GameWorld.worldTime.SetTime(tick, m_PredictedTime.tickInterval);
//                m_PlayerModule.RetrieveCommand(m_GameWorld.worldTime.tick);
//                PredictionUpdate();
//#if UNITY_EDITOR                 
//                // We only want to store "full" tick to we use m_PredictedTime.tick-1 (as current can be fraction of tick)
//                m_ReplicatedEntityModule.StorePredictedState(tick, m_PredictedTime.tick - 1);
//#endif                
//            }

//            // PREDICT CURRENT TICK. Update current tick using duration of current tick
//            m_GameWorld.worldTime = m_PredictedTime;
//            m_PlayerModule.RetrieveCommand(m_GameWorld.worldTime.tick);
//            // Dont update systems with close to zero time. 
//            if (m_GameWorld.worldTime.tickDuration > 0.008f)
//            {
//                PredictionUpdate();
//            }
//            //#if UNITY_EDITOR                 
//            //            m_ReplicatedEntityModule.StorePredictedState(m_PredictedTime.tick, m_PredictedTime.tick);
//            //#endif                
//        }

//        m_ProjectileModule.FinalizePredictedMovement();

//        m_GameModeSystem.Update();

//        // Update Presentation
//        m_GameWorld.worldTime = m_PredictedTime;
//        m_CharacterModule.UpdatePresentation();
//        m_DestructiblePropSystemClient.Update();
//        m_TeleporterSystemClient.Update();


//        m_GameWorld.worldTime = m_RenderTime;

//        // Handle despawns
//        m_HandlePresentationOwnerDespawn.Update();
//        m_CharacterModule.HandleDepawns(); // TODO (mogensh) this destroys presentations and needs to be done first so its picked up. We need better way of handling destruction ordering
//        m_ProjectileModule.HandleProjectileDespawn();
//        m_HandleNamePlateOwnerDespawn.Update();
//        m_TwistSystem.HandleDespawning();
//        m_FanSystem.HandleDespawning();
//        m_ragdollSystem.HandleDespawning();
//        m_HitCollisionModule.HandleDespawn();
//        m_TranslateScaleSystem.HandleDepawning();
//        m_GameWorld.ProcessDespawns();

//#if UNITY_EDITOR

//        if (m_GameWorld.GetEntityManager().Exists(m_localPlayer.controlledEntity) &&
//            m_GameWorld.GetEntityManager().HasComponent<UserCommandComponentData>(m_localPlayer.controlledEntity))
//        {
//            var userCommand = m_GameWorld.GetEntityManager().GetComponentData<UserCommandComponentData>(m_localPlayer.controlledEntity);
//            m_ReplicatedEntityModule.FinalizedStateHistory(m_PredictedTime.tick - 1, m_NetworkClient.serverTime, ref userCommand.command);
//        }
//#endif

    }

    //public void LateUpdate(ChatSystemClient chatSystem, float frameDuration)
    //{
    //    m_GameWorld.worldTime = m_RenderTime;
        //m_HitCollisionModule.StoreColliderState();

        //m_ragdollSystem.LateUpdate();

        //m_TranslateScaleSystem.Schedule();
        //var twistSystemHandle = m_TwistSystem.Schedule();
        //m_FanSystem.Schedule(twistSystemHandle);

        //var teamId = -1;
        //bool showScorePanel = false;
        //if (m_localPlayer != null && m_localPlayer.playerState != null && m_localPlayer.playerState.controlledEntity != Entity.Null)
        //{
        //    teamId = m_localPlayer.playerState.teamIndex;

        //    if (m_GameWorld.GetEntityManager().HasComponent<HealthStateData>(m_localPlayer.playerState.controlledEntity))
        //    {
        //        var healthState = m_GameWorld.GetEntityManager()
        //            .GetComponentData<HealthStateData>(m_localPlayer.playerState.controlledEntity);

        //        // Only show score board when alive
        //        showScorePanel = healthState.health <= 0;
        //    }
        //}
        //// TODO (petera) fix this hack
        //chatSystem.UpdateLocalTeamIndex(teamId);


        //m_ItemModule.LateUpdate();


        //m_CharacterModule.CameraUpdate();
        //m_PlayerModule.CameraUpdate();

        //m_CharacterModule.LateUpdate();

        //m_GameWorld.worldTime = m_RenderTime;
        //m_ProjectileModule.UpdateClientProjectilesNonPredicted();

        //m_GameWorld.worldTime = m_PredictedTime;
        //m_ProjectileModule.UpdateClientProjectilesPredicted();

        //m_ApplyGrenadePresentation.Update();

        //m_EffectModule.ClientUpdate();

        //m_UpdateNamePlates.Update();

        //if (Game.game.clientFrontend != null)
        //{
        //    m_ClientFrontendUpdate.Update();
        //    Game.game.clientFrontend.SetShowScorePanel(showScorePanel);
        //}

        //m_TranslateScaleSystem.Complete();
        //m_FanSystem.Complete();

    //}

    //bool IsPredictionAllowed()
    //{
    //    if (!m_PlayerModule.PlayerStateReady)
    //    {
    //        GameDebug.Log("No predict! No player state.");
    //        return false;
    //    }

    //    if (!m_PlayerModule.IsControllingEntity)
    //    {
    //        GameDebug.Log("No predict! No controlled entity.");
    //        return false;
    //    }

    //    if (m_PredictedTime.tick <= m_NetworkClient.serverTime)
    //    {
    //        GameDebug.Log("No predict! Predict time not ahead of server tick! " + GetFramePredictInfo());
    //        return false;
    //    }

    //    if (!m_PlayerModule.HasCommands(m_NetworkClient.serverTime + 1, m_PredictedTime.tick))
    //    {
    //        GameDebug.Log("No predict! No commands available. " + GetFramePredictInfo());
    //        return false;
    //    }

    //    return true;
    //}

    //string GetFramePredictInfo()
    //{
    //    int firstCommandTick;
    //    int lastCommandTick;
    //    m_PlayerModule.GetBufferedCommandsTick(out firstCommandTick, out lastCommandTick);

    //    return string.Format("Last server:{0} predicted:{1} buffer:{2}->{3} time since snap:{4}  rtt avr:{5}",
    //        m_NetworkClient.serverTime, m_PredictedTime.tick,
    //        firstCommandTick, lastCommandTick,
    //        m_NetworkClient.timeSinceSnapshot, m_NetworkStatistics.rtt.average);
    //}



    //public LocalPlayer RegisterLocalPlayer(int playerId)
    //{
    //    m_ReplicatedEntityModule.SetLocalPlayerId(playerId);
    //    m_localPlayer = m_PlayerModule.RegisterLocalPlayer(playerId, m_NetworkClient);
    //    return m_localPlayer;
    //}

    //public ISnapshotConsumer GetSnapshotConsumer()
    //{
    //    return m_ReplicatedEntityModule;
    //}

    //void PredictionRollback()
    //{
    //    m_ReplicatedEntityModule.Rollback();
    //}

    //void PredictionUpdate()
    //{
    //    m_SpectatorCamModule.Update();

    //    m_CharacterModule.AbilityRequestUpdate();

    //    m_CharacterModule.MovementStart();
    //    m_CharacterModule.MovementResolve();

    //    m_CharacterModule.AbilityStart();
    //    m_CharacterModule.AbilityResolve();

    //}

    //void HandleTime(float frameDuration)
    //{
    //    // Update tick rate (this will only change runtime in test scenarios)
    //    // TODO (petera) consider use ConfigVars with Server flag for this
    //    if (m_NetworkClient.serverTickRate != m_PredictedTime.tickRate)
    //    {
    //        m_PredictedTime.tickRate = m_NetworkClient.serverTickRate;
    //        m_RenderTime.tickRate = m_NetworkClient.serverTickRate;
    //    }

    //    // Sample input into current command
    //    //  The time passed in here is used to calculate the amount of rotation from stick position
    //    //  The command stores final view direction
    //    bool chatOpen = Game.game.clientFrontend != null && Game.game.clientFrontend.chatPanel.isOpen;
    //    bool userInputEnabled = Game.GetMousePointerLock() && !chatOpen;
    //    m_PlayerModule.SampleInput(userInputEnabled, Time.deltaTime, m_RenderTime.tick);


    //    int prevTick = m_PredictedTime.tick;

    //    // Increment time
    //    var deltaPredictedTime = frameDuration * frameTimeScale;
    //    m_PredictedTime.AddDuration(deltaPredictedTime);

    //    // Adjust time to be synchronized with server
    //    int preferredBufferedCommandCount = 2;
    //    int preferredTick = m_NetworkClient.serverTime + (int)(((m_NetworkClient.timeSinceSnapshot + m_NetworkStatistics.rtt.average) / 1000.0f) * m_GameWorld.worldTime.tickRate) + preferredBufferedCommandCount;

    //    bool resetTime = false;
    //    if (!resetTime && m_PredictedTime.tick < preferredTick - 3)
    //    {
    //        GameDebug.Log(string.Format("Client hard catchup ... "));
    //        resetTime = true;
    //    }

    //    if (!resetTime && m_PredictedTime.tick > preferredTick + 6)
    //    {
    //        GameDebug.Log(string.Format("Client hard slowdown ... "));
    //        resetTime = true;
    //    }

    //    frameTimeScale = 1.0f;
    //    if (resetTime)
    //    {
    //        GameDebug.Log(string.Format("CATCHUP ({0} -> {1})", m_PredictedTime.tick, preferredTick));

    //        m_NetworkStatistics.notifyHardCatchup = true;
    //        m_GameWorld.nextTickTime = Game.frameTime;
    //        m_PredictedTime.tick = preferredTick;
    //        m_PredictedTime.SetTime(preferredTick, 0);

    //    }
    //    else
    //    {
    //        int bufferedCommands = m_NetworkClient.lastAcknowlegdedCommandTime - m_NetworkClient.serverTime;
    //        if (bufferedCommands < preferredBufferedCommandCount)
    //            frameTimeScale = 1.01f;

    //        if (bufferedCommands > preferredBufferedCommandCount)
    //            frameTimeScale = 0.99f;
    //    }

    //    // Increment interpolation time
    //    m_RenderTime.AddDuration(frameDuration * frameTimeScale);

    //    // Force interp time to not exeede server time
    //    if (m_RenderTime.tick >= m_NetworkClient.serverTime)
    //    {
    //        m_RenderTime.SetTime(m_NetworkClient.serverTime, 0);
    //    }

    //    // hard catchup
    //    if (m_RenderTime.tick < m_NetworkClient.serverTime - 10)
    //    {
    //        m_RenderTime.SetTime(m_NetworkClient.serverTime - 8, 0);
    //    }

    //    // Throttle up to catch up
    //    if (m_RenderTime.tick < m_NetworkClient.serverTime - 1)
    //    {
    //        m_RenderTime.AddDuration(frameDuration * 0.01f);
    //    }

    //    // If predicted time has entered a new tick the stored commands should be sent to server 
    //    if (m_PredictedTime.tick > prevTick)
    //    {
    //        var oldestCommandToSend = Mathf.Max(prevTick, m_PredictedTime.tick - NetworkConfig.commandClientBufferSize);
    //        for (int tick = oldestCommandToSend; tick < m_PredictedTime.tick; tick++)
    //        {
    //            m_PlayerModule.StoreCommand(tick);
    //            m_PlayerModule.SendCommand(tick);
    //        }

    //        m_PlayerModule.ResetInput(userInputEnabled);
    //        m_PlayerModule.StoreCommand(m_PredictedTime.tick);
    //    }

    //    // Store command
    //    m_PlayerModule.StoreCommand(m_PredictedTime.tick);
    //}
}