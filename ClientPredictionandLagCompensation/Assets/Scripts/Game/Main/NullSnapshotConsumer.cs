﻿using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using UnityEngine.Profiling;
using UnityEngine.Ucg.Matchmaking;
using System;

public class NullSnapshotConsumer : ISnapshotConsumer
{
    public void ProcessEntityDespawns(int serverTime, List<int> despawns)
    {
    }

    public void ProcessEntitySpawn(int serverTime, int id, ushort typeId)
    {
    }

    public void ProcessEntityUpdate(int serverTime, int id, ref NetworkReader reader)
    {
    }
}
