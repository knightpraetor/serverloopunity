﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Game : MonoBehaviour
{
    public static Game game;

    [ConfigVar(Name = "server.tickrate", DefaultValue = "60", Description = "Tickrate for server", Flags = ConfigVar.Flags.ServerInfo)]
    public static ConfigVar serverTickRate;

    [ConfigVar(Name = "debug.catchloop", DefaultValue = "1", Description = "Catch exceptions in gameloop and pause game", Flags = ConfigVar.Flags.None)]
    public static ConfigVar debugCatchLoop;

    public Camera BootCamera;

    public LevelManager levelManager;
    //public SQPClient sqpClient;

    // Global camera handling
    List<Camera> m_CameraStack = new List<Camera>();

    public static GameConfiguration config;

    List<IGameLoop> m_gameLoops = new List<IGameLoop>();

    List<Type> m_RequestedGameLoopTypes = new List<System.Type>();
    private List<string[]> m_RequestedGameLoopArguments = new List<string[]>();

    bool m_ErrorState = false;

    public string ServerPort;
    public string SQPPort;

    public EditorMode Mode;

    long m_StopwatchFrequency;
    public static double frameTime;
    System.Diagnostics.Stopwatch m_Clock;

    public ClientFrontend clientFrontend;

    public static System.Diagnostics.Stopwatch Clock
    {
        get { return game.m_Clock; }
    }
    public string buildId
    {
        get { return _buildId; }
    }
    string _buildId = "NoBuild";

    public interface IGameLoop
    {
        bool Init(string[] args);
        void Shutdown();

        void Update();
        void FixedUpdate();
        void LateUpdate();
    }
   

    public enum EditorMode
    {
        Client,
        Server
    }

    private void Awake()
    {
        GameDebug.Assert(game == null);
        DontDestroyOnLoad(gameObject);
        game = this;

        m_StopwatchFrequency = System.Diagnostics.Stopwatch.Frequency;
        m_Clock = new System.Diagnostics.Stopwatch();
        m_Clock.Start();

        SetLogFilePath();
        ConfigVar.Init();

        //no frame cap
        //Application.targetFrameRate = -1;
        Application.targetFrameRate = serverTickRate.IntValue;
        QualitySettings.vSyncCount = 0; // Needed to make targetFramerate work;

        //RenderSettings.Init();

        //InitializeSoundSystem

        //Instantiate ClientFront prefab

        //SQPClient
        //sqpClient = new SQP.SQPClient();


        //SimpleBundleManager
        SimpleBundleManager.Init();
        GameDebug.Log("SimpleBundleManager initialized");

        //LevelManager
        levelManager = new LevelManager();
        levelManager.Init();

        //InputSystem


        PushCamera(BootCamera);

    }

   
    // Start is called before the first frame update
    void Start()
    {
        switch (Mode) 
        {
            case EditorMode.Client:
                break;
            case EditorMode.Server:
                break;
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        // Verify if camera was somehow destroyed and pop it
        if (m_CameraStack.Count > 1 && m_CameraStack[m_CameraStack.Count - 1] == null)
        {
            PopCamera(null);
        }

        frameTime = (double)m_Clock.ElapsedTicks / m_StopwatchFrequency;


        // Switch game loop if needed
        if (m_RequestedGameLoopTypes.Count > 0)
        {
            // Multiple running gameloops only allowed in editor
#if !UNITY_EDITOR
            ShutdownGameLoops();
#endif
            bool initSucceeded = false;
            for (int i = 0; i < m_RequestedGameLoopTypes.Count; i++)
            {
                try
                {
                    IGameLoop gameLoop = (IGameLoop)System.Activator.CreateInstance(m_RequestedGameLoopTypes[i]);
                    initSucceeded = gameLoop.Init(m_RequestedGameLoopArguments[i]);
                    if (!initSucceeded)
                        break;

                    m_gameLoops.Add(gameLoop);
                }
                catch (System.Exception e)
                {
                    GameDebug.Log(string.Format("Game loop initialization threw exception : ({0})\n{1}", e.Message, e.StackTrace));
                }
            }


            if (!initSucceeded)
            {
                ShutdownGameLoops();

                GameDebug.Log("Game loop initialization failed ... reverting to boot loop");
            }

            m_RequestedGameLoopTypes.Clear();
            m_RequestedGameLoopArguments.Clear();
        }

        try
        {
            if (!m_ErrorState)
            {
                foreach (var gameLoop in m_gameLoops)
                {
                    gameLoop.Update();
                }
                levelManager.Update();
            }
        }
        catch (System.Exception e)
        {
            HandleGameloopException(e);
            throw;
        }

    }
    void SetLogFilePath()
    {
        // If -logfile was passed, we try to put our own logs next to the engine's logfile
        var path = Application.dataPath;
        DirectoryInfo parentDir = Directory.GetParent(path);
        var engineLogFileLocation = parentDir.FullName;


        var logName = "game_" + DateTime.UtcNow.ToString("yyyyMMdd_HHmmss_fff");
        GameDebug.Init(engineLogFileLocation, logName);
    }

    public void PushCamera(Camera cam)
    {
        if (m_CameraStack.Count > 0)
            SetCameraEnabled(m_CameraStack[m_CameraStack.Count - 1], false);
        m_CameraStack.Add(cam);
        SetCameraEnabled(cam, true);
        //m_ExposureReleaseCount = 10;
    }

    public void PopCamera(Camera cam)
    {
        GameDebug.Assert(m_CameraStack.Count > 1, "Trying to pop last camera off stack!");
        GameDebug.Assert(cam == m_CameraStack[m_CameraStack.Count - 1]);
        if (cam != null)
            SetCameraEnabled(cam, false);
        m_CameraStack.RemoveAt(m_CameraStack.Count - 1);
        SetCameraEnabled(m_CameraStack[m_CameraStack.Count - 1], true);
    }

    public Camera TopCamera()
    {
        var c = m_CameraStack.Count;
        return c == 0 ? null : m_CameraStack[c - 1];
    }

    void SetCameraEnabled(Camera cam, bool enabled)
    {
        if (enabled)
        {
            //TODO look into render settings and HD Pipeline
            //RenderSettings.UpdateCameraSettings(cam);
        }
            

        cam.enabled = enabled;
        var audioListener = cam.GetComponent<AudioListener>();
        if (audioListener != null)
        {
            audioListener.enabled = enabled;
            //if (SoundSystem != null)
            //    SoundSystem.SetCurrentListener(enabled ? audioListener : null);
        }
    }

    public void RequestGameLoop(System.Type type, string[] args)
    {
        GameDebug.Assert(typeof(IGameLoop).IsAssignableFrom(type));

        m_RequestedGameLoopTypes.Add(type);
        m_RequestedGameLoopArguments.Add(args);
        GameDebug.Log("Game loop " + type + " requested");
    }

    public static T GetGameLoop<T>() where T : class
    {
        if (game == null)
            return null;
        foreach (var gameLoop in game.m_gameLoops)
        {
            T result = gameLoop as T;
            if (result != null)
                return result;
        }
        return null;
    }
    public static int GameLoopCount
    {
        get { return game == null ? 0 : 1; }
    }

    void ShutdownGameLoops()
    {
        foreach (var gameLoop in m_gameLoops)
            gameLoop.Shutdown();
        m_gameLoops.Clear();
    }

    void HandleGameloopException(System.Exception e)
    {
        if (debugCatchLoop.IntValue > 0)
        {
            GameDebug.Log("EXCEPTION " + e.Message + "\n" + e.StackTrace);
            //Console.SetOpen(true);
            m_ErrorState = true;
        }
    }
}
