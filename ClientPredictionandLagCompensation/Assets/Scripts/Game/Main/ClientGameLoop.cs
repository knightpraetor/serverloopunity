﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Ucg.Matchmaking;

public class ClientGameLoop : Game.IGameLoop
{
    [ConfigVar(Name = "client.updaterate", DefaultValue = "30000", Description = "Max bytes/sec client wants to receive", Flags = ConfigVar.Flags.ClientInfo)]
    public static ConfigVar clientUpdateRate;
    [ConfigVar(Name = "client.updateinterval", DefaultValue = "3", Description = "Snapshot sendrate requested by client", Flags = ConfigVar.Flags.ClientInfo)]
    public static ConfigVar clientUpdateInterval;

    [ConfigVar(Name = "client.playername", DefaultValue = "Noname", Description = "Name of player", Flags = ConfigVar.Flags.ClientInfo | ConfigVar.Flags.Save)]
    public static ConfigVar clientPlayerName;

    [ConfigVar(Name = "client.matchmaker", DefaultValue = "0.0.0.0:80", Description = "Address of matchmaker", Flags = ConfigVar.Flags.None)]
    public static ConfigVar clientMatchmaker;

    public ClientFrontend clientFrontend;

    enum ClientState
    {
        Browsing,
        Connecting,
        Loading,
        Playing,
    }
    StateMachine<ClientState> m_StateMachine;

    ClientState m_ClientState;

    GameWorld m_GameWorld;

    SocketTransport m_NetworkTransport;

    NetworkClient m_NetworkClient;

    //LocalPlayer m_LocalPlayer;
    //PlayerSettings m_requestedPlayerSettings = new PlayerSettings();
    //bool m_playerSettingsUpdated;

    //NetworkStatisticsClient m_NetworkStatistics;
    //ChatSystemClient m_ChatSystem;

    ClientGameWorld m_clientWorld;
    BundledResourceManager m_resourceSystem;

    string m_LevelName;

    string m_DisconnectReason = null;
    string m_GameMessage = "Welcome to the sample game!";

    double m_lastFrameTime;
    bool m_predictionEnabled = true;
    bool m_performGameWorldLateUpdate;

    bool m_useMatchmaking = false;
    Matchmaker m_matchmaker;

    [ConfigVar(Name = "client.showtickinfo", DefaultValue = "0", Description = "Show tick info")]
    static ConfigVar m_showTickInfo;
    [ConfigVar(Name = "client.showcommandinfo", DefaultValue = "0", Description = "Show command info")]
    static ConfigVar m_showCommandInfo;

    string targetServer = "";
    int connectRetryCount;

    public bool Init(string[] args)
    {
        m_StateMachine = new StateMachine<ClientState>();
        m_StateMachine.Add(ClientState.Browsing, EnterBrowsingState, UpdateBrowsingState, LeaveBrowsingState);
        m_StateMachine.Add(ClientState.Connecting, EnterConnectingState, UpdateConnectingState, null);
        m_StateMachine.Add(ClientState.Loading, EnterLoadingState, UpdateLoadingState, null);
        m_StateMachine.Add(ClientState.Playing, EnterPlayingState, UpdatePlayingState, LeavePlayingState);


#if UNITY_EDITOR
        Game.game.levelManager.UnloadLevel();
#endif
        m_GameWorld = new GameWorld("ClientWorld");



        m_NetworkTransport = new SocketTransport();
        m_NetworkClient = new NetworkClient(m_NetworkTransport);

        if (Application.isEditor || Game.game.buildId == "AutoBuild")
            NetworkClient.clientVerifyProtocol.Value = "0";

        m_NetworkClient.UpdateClientConfig();
        //m_NetworkStatistics = new NetworkStatisticsClient(m_NetworkClient);
        //m_ChatSystem = new ChatSystemClient(m_NetworkClient);

        GameDebug.Log("Network client initialized");

        //m_requestedPlayerSettings.playerName = clientPlayerName.Value;
        //m_requestedPlayerSettings.teamId = -1;

        //Console.AddCommand("disconnect", CmdDisconnect, "Disconnect from server if connected", this.GetHashCode());
        //Console.AddCommand("prediction", CmdTogglePrediction, "Toggle prediction", this.GetHashCode());
        //Console.AddCommand("runatserver", CmdRunAtServer, "Run command at server", this.GetHashCode());
        //Console.AddCommand("respawn", CmdRespawn, "Force a respawn", this.GetHashCode());
        //Console.AddCommand("nextchar", CmdNextChar, "Select next character", this.GetHashCode());
        //Console.AddCommand("nextteam", CmdNextTeam, "Select next character", this.GetHashCode());
        //Console.AddCommand("spectator", CmdSpectator, "Select spectator cam", this.GetHashCode());
        //Console.AddCommand("matchmake", CmdMatchmake, "matchmake <hostname[:port]/{projectid}>: Find and join a server", this.GetHashCode());

        if (args.Length > 0)
        {
            targetServer = args[0];
            m_StateMachine.SwitchTo(ClientState.Connecting);
        }
        else
            m_StateMachine.SwitchTo(ClientState.Browsing);

        GameDebug.Log("Client initialized");

        return true;
    }
    public void Shutdown()
    {
        //GameDebug.Log("ClientGameLoop shutdown");
        //Console.RemoveCommandsWithTag(this.GetHashCode());

        //m_StateMachine.Shutdown();

        //m_NetworkClient.Shutdown();
        //m_NetworkTransport.Shutdown();

        //m_GameWorld.Shutdown();
    }

    void EnterBrowsingState()
    {
        GameDebug.Assert(m_clientWorld == null);
        m_ClientState = ClientState.Browsing;
    }

    void UpdateBrowsingState()
    {
        if (m_useMatchmaking)
        {
            m_matchmaker?.Update();
        }
    }
    void LeaveBrowsingState()
    {
    }

    void EnterConnectingState()
    {
        GameDebug.Assert(m_ClientState == ClientState.Browsing, "Expected ClientState to be browsing");
        GameDebug.Assert(m_clientWorld == null, "Expected ClientWorld to be null");
        GameDebug.Assert(m_NetworkClient.connectionState == NetworkClient.ConnectionState.Disconnected, "Expected network connectionState to be disconnected");

        m_ClientState = ClientState.Connecting;
        connectRetryCount = 0;
    }

    void UpdateConnectingState()
    {
        switch (m_NetworkClient.connectionState)
        {
            case NetworkClient.ConnectionState.Connected:
                m_GameMessage = "Waiting for map info";
                break;
            case NetworkClient.ConnectionState.Connecting:
                // Do nothing; just wait for either success or failure
                break;
            case NetworkClient.ConnectionState.Disconnected:
                if (connectRetryCount < 2)
                {
                    connectRetryCount++;
                    m_GameMessage = string.Format("Trying to connect to {0} (attempt #{1})...", targetServer, connectRetryCount);
                    GameDebug.Log(m_GameMessage);
                    m_NetworkClient.Connect(targetServer);
                }
                else
                {
                    m_GameMessage = "Failed to connect to server";
                    GameDebug.Log(m_GameMessage);
                    m_NetworkClient.Disconnect();
                    m_StateMachine.SwitchTo(ClientState.Browsing);
                }
                break;
        }
    }

    void EnterLoadingState()
    {
        //if (Game.game.clientFrontend != null)
        //    Game.game.clientFrontend.ShowMenu(ClientFrontend.MenuShowing.None);

        //Console.SetOpen(false);

        //GameDebug.Assert(m_clientWorld == null);
        //GameDebug.Assert(m_NetworkClient.isConnected);

        //m_requestedPlayerSettings.playerName = clientPlayerName.Value;
        //m_requestedPlayerSettings.characterType = (short)Game.characterType.IntValue;
        //m_playerSettingsUpdated = true;

        m_ClientState = ClientState.Loading;
    }

    void UpdateLoadingState()
    {
        // Handle disconnects
        if (!m_NetworkClient.isConnected)
        {
            m_GameMessage = m_DisconnectReason != null ? string.Format("Disconnected from server ({0})", m_DisconnectReason) : "Disconnected from server (lost connection)";
            m_DisconnectReason = null;
            m_StateMachine.SwitchTo(ClientState.Browsing);
        }

        // Wait until we got level info
        if (m_LevelName == null)
            return;

        // Load if we are not already loading
        var level = Game.game.levelManager.currentLevel;
        if (level == null || level.name != m_LevelName)
        {
            if (!Game.game.levelManager.LoadLevel(m_LevelName))
            {
                m_DisconnectReason = string.Format("could not load requested level '{0}'", m_LevelName);
                m_NetworkClient.Disconnect();
                return;
            }
            level = Game.game.levelManager.currentLevel;
        }

        // Wait for level to be loaded
        if (level.state == LevelState.Loaded)
            m_StateMachine.SwitchTo(ClientState.Playing);
    }

    void EnterPlayingState()
    {
        //GameDebug.Assert(m_clientWorld == null && Game.game.levelManager.IsCurrentLevelLoaded());

        //m_GameWorld.RegisterSceneEntities();

        //m_resourceSystem = new BundledResourceManager(m_GameWorld, "BundledResources/Client");

        //m_clientWorld = new ClientGameWorld(m_GameWorld, m_NetworkClient, m_NetworkStatistics, m_resourceSystem);
        //m_clientWorld.PredictionEnabled = m_predictionEnabled;

        //m_LocalPlayer = m_clientWorld.RegisterLocalPlayer(m_NetworkClient.clientId);

        //m_NetworkClient.QueueEvent((ushort)GameNetworkEvents.EventType.PlayerReady, true, (ref NetworkWriter data) => { });

        //m_ClientState = ClientState.Playing;
    }

    void LeavePlayingState()
    {
        //m_resourceSystem.Shutdown();

        //m_LocalPlayer = null;

        //m_clientWorld.Shutdown();
        //m_clientWorld = null;

        //// TODO (petera) replace this with a stack of levels or similar thing. For now we just load the menu no matter what
        ////Game.game.levelManager.UnloadLevel();
        ////Game.game.levelManager.LoadLevel("level_menu");

        //m_resourceSystem.Shutdown();

        //m_GameWorld.Shutdown();
        //m_GameWorld = new GameWorld("ClientWorld");

        //if (Game.game.clientFrontend != null)
        //{
        //    Game.game.clientFrontend.Clear();
        //    Game.game.clientFrontend.ShowMenu(ClientFrontend.MenuShowing.None);
        //}

        //Game.game.levelManager.LoadLevel("level_menu");

        //GameDebug.Log("Left playingstate");
    }

    void UpdatePlayingState()
    {
        //// Handle disconnects
        //if (!m_NetworkClient.isConnected)
        //{
        //    m_GameMessage = m_DisconnectReason != null ? string.Format("Disconnected from server ({0})", m_DisconnectReason) : "Disconnected from server (lost connection)";
        //    m_StateMachine.SwitchTo(ClientState.Browsing);
        //    return;
        //}

        //// (re)send client info if any of the configvars that contain clientinfo has changed
        //if ((ConfigVar.DirtyFlags & ConfigVar.Flags.ClientInfo) == ConfigVar.Flags.ClientInfo)
        //{
        //    m_NetworkClient.UpdateClientConfig();
        //    ConfigVar.DirtyFlags &= ~ConfigVar.Flags.ClientInfo;
        //}

        //if (Game.Input.GetKeyUp(KeyCode.H))
        //{
        //    RemoteConsoleCommand("nextchar");
        //}

        //if (Game.Input.GetKeyUp(KeyCode.T))
        //    CmdNextTeam(null);

        //float frameDuration = m_lastFrameTime != 0 ? (float)(Game.frameTime - m_lastFrameTime) : 0;
        //m_lastFrameTime = Game.frameTime;

        //m_clientWorld.Update(frameDuration);
        //m_performGameWorldLateUpdate = true;
    }

    public void FixedUpdate()
    {
        
    }

  

    public void LateUpdate()
    {
        //if (m_clientWorld != null && m_performGameWorldLateUpdate)
        //{
        //    m_performGameWorldLateUpdate = false;
        //    m_clientWorld.LateUpdate(m_ChatSystem, Time.deltaTime);
        //}

        //ShowInfoOverlay(0, 1);
    }

    public void Update()
    {
        throw new NotImplementedException();
    }
}

