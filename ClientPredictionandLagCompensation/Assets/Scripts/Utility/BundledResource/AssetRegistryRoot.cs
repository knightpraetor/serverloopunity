﻿using UnityEngine;

[CreateAssetMenu(fileName = "AssetRegistryRoot", menuName = "Resource/AssetRegistryRoot", order = 10000)]
public class AssetRegistryRoot : ScriptableObject
{
    public bool serverBuild;
    public ScriptableObject[] assetRegistries;
}
