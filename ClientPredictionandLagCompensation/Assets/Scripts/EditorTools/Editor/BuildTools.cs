﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class BuildTools
{
    public static void CopyDirectory(string SourcePath, string DestinationPath)
    {
        //Create all of the directories
        foreach (string dirPath in Directory.GetDirectories(SourcePath, "*",
            SearchOption.AllDirectories))
            Directory.CreateDirectory(dirPath.Replace(SourcePath, DestinationPath));

        //Copy all the files & Replaces any files with the same name
        foreach (string newPath in Directory.GetFiles(SourcePath, "*.*",
            SearchOption.AllDirectories))
            File.Copy(newPath, newPath.Replace(SourcePath, DestinationPath), true);
    }

    public static UnityEditor.Build.Reporting.BuildReport BuildGame(string buildPath, string exeName, BuildTarget target,
        BuildOptions opts, string buildId, bool il2cpp)
    {
        var levels = new string[]
        {
            "Assets/Scenes/bootstrapper.unity",
            "Assets/Scenes/empty.unity"
        };

        var exePathName = buildPath + "/" + exeName;

        Debug.Log("Building: " + exePathName);
        Directory.CreateDirectory(buildPath);

        // Set all files to be writeable (As Unity 2017.1 sets them to read only)
        string fullBuildPath = Directory.GetCurrentDirectory() + "/" + buildPath;
        string[] fileNames = Directory.GetFiles(fullBuildPath, "*.*", SearchOption.AllDirectories);

        //Contentpipeline compile player scripts

        foreach (var fileName in fileNames)
        {
            FileAttributes attributes = File.GetAttributes(fileName);
            attributes &= ~FileAttributes.ReadOnly;
            File.SetAttributes(fileName, attributes);
        }

        string bundlePathSrc = buildPath + "/" + SimpleBundleManager.assetBundleFolder;
        string bundlePathDst = "Assets/StreamingAssets/" + SimpleBundleManager.assetBundleFolder;
        if (target == BuildTarget.PS4)
        {
            if (!Directory.Exists(bundlePathSrc))
            {
                EditorUtility.DisplayDialog("No bundles found", "No Asset Bundles found. Please build them first",
                    "Ok");
                return null;
            }

            CopyDirectory(bundlePathSrc, bundlePathDst);
        }

        var monoDirs = Directory.GetDirectories(fullBuildPath).Where(s => s.Contains("MonoBleedingEdge"));
        var il2cppDirs = Directory.GetDirectories(fullBuildPath).Where(s => s.Contains("BackUpThisFolder_ButDontShipItWithYourGame"));
        var clearFolder = (il2cpp && monoDirs.Count() > 0) || (!il2cpp && il2cppDirs.Count() > 0);
        if (clearFolder)
        {
            Debug.Log(" deleting old folders ..");
            foreach (var file in Directory.GetFiles(fullBuildPath))
                File.Delete(file);
            foreach (var dir in monoDirs)
                Directory.Delete(dir, true);
            foreach (var dir in il2cppDirs)
                Directory.Delete(dir, true);
            foreach (var dir in Directory.GetDirectories(fullBuildPath).Where(s => s.EndsWith("_Data")))
                Directory.Delete(dir, true);
        }

        if (il2cpp)
        {
            UnityEditor.PlayerSettings.SetScriptingBackend(BuildTargetGroup.Standalone, ScriptingImplementation.IL2CPP);
            UnityEditor.PlayerSettings.SetIl2CppCompilerConfiguration(BuildTargetGroup.Standalone, Il2CppCompilerConfiguration.Release);
        }
        else
        {
            UnityEditor.PlayerSettings.SetScriptingBackend(BuildTargetGroup.Standalone, ScriptingImplementation.Mono2x);
        }

        /// Colossal hack to work around build postprocessing expecting everything to be writable in the unity
        /// installation, but if people have unity in p4 it will be readonly.
        var editorHome = EditorApplication.applicationPath.BeforeLast("/") + "/Data/PlaybackEngines/windowsstandalonesupport";
        Debug.Log("Checking for read/only files in standalone players");
        if (Directory.Exists(editorHome))
        {
            var files = Directory.GetFiles(editorHome, "*.*", SearchOption.AllDirectories);
            foreach (var f in files)
            {
                var attr = File.GetAttributes(f);
                if ((attr & FileAttributes.ReadOnly) != 0)
                {
                    attr = attr & ~FileAttributes.ReadOnly;
                    Debug.Log("Setting " + f + " to read/write");
                    File.SetAttributes(f, attr);
                }
            }
        }
        Debug.Log("Done.");

        Environment.SetEnvironmentVariable("BUILD_ID", buildId, EnvironmentVariableTarget.Process);
        var result = BuildPipeline.BuildPlayer(levels, exePathName, target, opts);
        Environment.SetEnvironmentVariable("BUILD_ID", "", EnvironmentVariableTarget.Process);

        if (target == BuildTarget.PS4)
        {
            Directory.Delete(bundlePathDst, true);
        }


        Debug.Log(" ==== Build Done =====");


        var stepCount = result.steps.Count();
        Debug.Log(" Steps:" + stepCount);
        for (var i = 0; i < stepCount; i++)
        {
            var step = result.steps[i];
            Debug.Log("-- " + (i + 1) + "/" + stepCount + " " + step.name + " " + step.duration.Seconds + "s --");
            foreach (var msg in step.messages)
                Debug.Log(msg.content);
        }

        return result;
    }

    public static List<LevelInfo> LoadLevelInfos()
    {
        return LoadAssetsOfType<LevelInfo>();
    }

    public static List<T> LoadAssetsOfType<T>() where T : UnityEngine.Object
    {
        var result = new List<T>();
        var assets = AssetDatabase.FindAssets("t:" + typeof(T).Name);
        foreach (var a in assets)
        {
            var path = AssetDatabase.GUIDToAssetPath(a);
            result.Add(AssetDatabase.LoadAssetAtPath<T>(path));
        }
        return result;
    }

    public static void BuildBundles(string bundlePath, BuildTarget target, bool buildBundledAssets, bool buildBundledLevels, bool force = false, List<LevelInfo> buildOnlyLevels = null)
    {
        Debug.Log("Scene cooking started");

        var path = bundlePath + "/" + SimpleBundleManager.assetBundleFolder;

        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);

        BuildAssetBundleOptions assetBundleOptions = BuildAssetBundleOptions.UncompressedAssetBundle;
        if (force)
        {
            Debug.Log("Forcing rebuild");
            assetBundleOptions |= BuildAssetBundleOptions.ForceRebuildAssetBundle;
        }

        if (buildBundledLevels)
            BuildLevelBundles(path, target, assetBundleOptions, buildOnlyLevels);

        if (buildBundledAssets)
        {
            //TODO: ADD BUndledResourceBuilder
            //BundledResourceBuilder.BuildBundles(path, target, assetBundleOptions);
        }
    }

    static AssetBundleBuild MakeSceneBundleBuild(UnityEngine.Object mainScene, string name)
    {
        var build = new AssetBundleBuild();
        build.assetBundleName = name;
        build.assetBundleVariant = "";

        var path = AssetDatabase.GetAssetPath(mainScene);
        var scenes = new List<string>();
        scenes.Add(path.ToLower());

        //TODO: ADD EditorLevelManager
        //if (EditorLevelManager.IsLayeredLevel(path))
        //{
        //    foreach (var l in EditorLevelManager.GetLevelLayers(path))
        //    {
        //        scenes.Add(l.ToLower());
        //    }
        //}

        build.assetNames = scenes.ToArray();
        return build;
    }


    static AssetBundleBuild MakeAssetBundleBuild(List<string> assets, string name)
    {
        var build = new AssetBundleBuild();
        build.assetBundleName = name;
        build.assetBundleVariant = "";
        build.assetNames = assets.ToArray();
        return build;
    }


    public static void BuildLevelBundles(string path, BuildTarget target, BuildAssetBundleOptions assetBundleOptions, List<LevelInfo> buildOnlyLevels = null)
    {
        var builds = new List<AssetBundleBuild>();
        foreach (var levelInfo in LoadLevelInfos())
        {
            if (buildOnlyLevels != null && !buildOnlyLevels.Contains(levelInfo))
                continue;
            if (!levelInfo.IncludeInBuild)
                continue;
            Debug.Log(" - adding level: " + AssetDatabase.GetAssetPath(levelInfo.MainScene));
            var build = MakeSceneBundleBuild(levelInfo.MainScene, levelInfo.name);
            builds.Add(build);
        }

        // TODO (petera) enable once we've done proper shared assets support
        //var dependencies = FindSharedDependencies(builds);
        //var sharedbuild = MakeAssetBundleBuild(dependencies, "shared_assets");
        //builds.Add(sharedbuild);

        // TODO (mogensh) Settle on what buildpipeline to use. LegacyBuildPipeline uses SBP internally and is faster.      
        //        LegacyBuildPipeline.BuildAssetBundles(path, builds.ToArray(), assetBundleOptions, EditorUserBuildSettings.activeBuildTarget);
        BuildPipeline.BuildAssetBundles(path, builds.ToArray(), assetBundleOptions, EditorUserBuildSettings.activeBuildTarget);

        // Set write time so tools can show time since build
        Directory.SetLastWriteTime(path, DateTime.Now);

        Debug.Log("Scene cooking done");
    }

}
