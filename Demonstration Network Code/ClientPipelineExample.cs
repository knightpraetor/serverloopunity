﻿using Unity.Collections;
using Unity.Networking.Transport;
using Unity.Networking.Transport.Utilities;

public class Client
{
    NetworkDriver m_DriverHandle;
    NetworkPipeline m_Pipeline;

    const int k_PacketSize = 256;

    // Connection establishment omitted
    public NetworkConnection m_ConnectionToServer;

    public void Configure()
    {
        // Driver can be used as normal
        m_DriverHandle = NetworkDriver.Create(new SimulatorUtility.Parameters { MaxPacketSize = k_PacketSize, MaxPacketCount = 30, PacketDelayMs = 100 });
        // Driver now knows about this pipeline and can explicitly be asked to send packets through it (by default it sends directly)
        m_Pipeline = m_DriverHandle.CreatePipeline(typeof(UnreliableSequencedPipelineStage), typeof(SimulatorPipelineStage));
    }

    public void SendMessage(NativeArray<byte> someData)
    {
        // Send using the pipeline created in Configure()
        var writer = m_DriverHandle.BeginSend(m_Pipeline, m_ConnectionToServer);
        writer.WriteBytes(someData);
        m_DriverHandle.EndSend(writer);
    }
}